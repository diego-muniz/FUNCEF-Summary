# Componente - FUNCEF-Summary

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#modulo)
6. [Uso](#uso)
7. [Parâmetros](#parâmetros)
8. [Desinstalação](#desinstalação)


## Descrição

- Apresenta as informações da tabela como quantidade de itens por página, total de itens, quantidade de páginas e combo para alterar a quantidade de itens por página.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-summary --save.
```

## Script

```html
<script src="bower_components/funcef-summary/dist/funcef-summary.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-summary/dist/funcef-summary.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funcef-summary dentro do módulo do Sistema.

```js
    angular
        .module('funcef-demo', ['funcef-summary']);
```

## Uso

```html
<ngf-summary filtro="vm.filtro" model-page="vm.filtro.pagination.number"></ngf-summary>
```

### Parâmetros:

- filtro (Obrigatório);
- model-page (Obrigatório);

## Desinstalação:

```
bower uninstall funcef-summary --save
```