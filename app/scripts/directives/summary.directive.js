﻿(function () {
    'use strict';

    angular
      .module('funcef-summary.directive')
      .directive('ngfSummary', ngfSummary);

    /* @ngInject */
    function ngfSummary() {
        return {
            restrict: 'EA',
            transclude: true,
            templateUrl: 'views/summary.view.html',
            controller: 'SummaryController',
            controllerAs: 'vm',
            scope: {
                filtro: '=',
                modelPage: '='
            },
            link: function (scope, element, attr, ctrl) {
                scope.$watch('filtro', function () {
                    ctrl.filtro = scope.filtro;
                });
            }
        };
    }
}());