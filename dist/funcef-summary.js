(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-summary.directive', []);
    angular.module('funcef-summary.controller', []);


    angular
    .module('funcef-summary', [
      'funcef-summary.directive',
      'funcef-summary.controller',
    ]);
})();;(function () {
    'use strict';

    angular.module('funcef-summary.controller')
        .controller('SummaryController', SummaryController);

    /* @ngInject */
    function SummaryController() {
        var vm = this;
        vm.calculaUltimoItem = calculaUltimoItem;
        vm.atualizar = atualizar;

        /////////

        function calculaUltimoItem() {
            if (!vm.filtro || !vm.filtro.pagination) {
                return false;
            }

            var ultimo = vm.filtro.pagination.start + vm.filtro.pagination.number;

            return (ultimo > vm.filtro.pagination.totalItemCount) ? vm.filtro.pagination.totalItemCount : ultimo;
        }

        function atualizar(qtd) {
            if (!vm.filtro || !vm.filtro.pagination) {
                return false;
            }

            vm.filtro.pagination.number = qtd;
        }
    }

}());;(function () {
    'use strict';

    angular
      .module('funcef-summary.directive')
      .directive('ngfSummary', ngfSummary);

    /* @ngInject */
    function ngfSummary() {
        return {
            restrict: 'EA',
            transclude: true,
            templateUrl: 'views/summary.view.html',
            controller: 'SummaryController',
            controllerAs: 'vm',
            scope: {
                filtro: '=',
                modelPage: '='
            },
            link: function (scope, element, attr, ctrl) {
                scope.$watch('filtro', function () {
                    ctrl.filtro = scope.filtro;
                });
            }
        };
    }
}());;angular.module('funcef-summary').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/summary.view.html',
    "<div class=\"row\"> <div class=\"col-sm-8\"> <label class=\"control-label pull-left\"> Registros por página: </label> <div class=\"col-md-2\"> <select class=\"form-control pull-left select-qtde-paginas\" name=\"registro-pagina\" ng-model=\"modelPage\"> <option ng-value=\"10\">10</option> <option ng-value=\"20\">20</option> <option ng-value=\"50\">50</option> <option ng-value=\"100\">100</option> </select> </div> </div> <div class=\"col-sm-4 text-right\"> Mostrando: <strong>{{ vm.filtro.pagination.start+1 | number:0 }}-{{ vm.calculaUltimoItem() | number:0 }}</strong> de <strong>{{ vm.filtro.pagination.totalItemCount | number:0 }}</strong> itens. </div> </div>"
  );

}]);
