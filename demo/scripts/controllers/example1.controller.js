﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = [];

    /* @ngInject */
    function Example1Controller() {
        var vm = this;

        vm.initListaFiltro = initListaFiltro;
        vm.listarResultado = [];
        vm.registrosPagina = 10;

        /////////

        function initListaFiltro(filtro) {
            if (filtro != undefined) {
                vm.filtroTabela = filtro;
            }
            buscar();
        }

        function buscar() {
        	vm.listarResultado = [];

            for (var i = vm.filtroTabela.pagination.start; i <= vm.filtroTabela.pagination.start + vm.registrosPagina; i++) {
            	vm.listarResultado.push({
            		Id: i,
            		Nome: 'Nome ' + i 
            	});
            }

            vm.filtroTabela.pagination.numberOfPages = Math.ceil(100 / vm.filtroTabela.pagination.number);
            vm.filtroTabela.pagination.totalItemCount = 100;
        }
    }
}());