﻿(function () {
    'use strict';

    angular.module('funcef-summary.controller')
        .controller('SummaryController', SummaryController);

    /* @ngInject */
    function SummaryController() {
        var vm = this;
        vm.calculaUltimoItem = calculaUltimoItem;
        vm.atualizar = atualizar;

        /////////

        function calculaUltimoItem() {
            if (!vm.filtro || !vm.filtro.pagination) {
                return false;
            }

            var ultimo = vm.filtro.pagination.start + vm.filtro.pagination.number;

            return (ultimo > vm.filtro.pagination.totalItemCount) ? vm.filtro.pagination.totalItemCount : ultimo;
        }

        function atualizar(qtd) {
            if (!vm.filtro || !vm.filtro.pagination) {
                return false;
            }

            vm.filtro.pagination.number = qtd;
        }
    }

}());