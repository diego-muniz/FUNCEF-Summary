﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Summary
    * @version 1.0.0
    * @Componente Summary 
    */
    angular.module('funcef-summary.directive', []);
    angular.module('funcef-summary.controller', []);


    angular
    .module('funcef-summary', [
      'funcef-summary.directive',
      'funcef-summary.controller',
    ]);
})();